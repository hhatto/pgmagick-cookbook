import sys
from pgmagick import Image, Blob

blob = Blob()
img = Image('exif-sample.jpg')
img.profile("*", blob)
img.write('exif-strip.jpg')
