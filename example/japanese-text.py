# coding: utf-8
from pgmagick.api import Image

img = Image((300, 200), 'plasma:green')
img.oilPaint(10)
img.font("/usr/share/fonts/truetype/ttf-japanese-gothic.ttf")
img.fontPointsize(30)
img.annotate('ようこそpgmagickへ!!', angle=30)
img.write('japanese-text.png')
