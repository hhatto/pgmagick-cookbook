from pgmagick.api import Image

img = Image('lena.jpg')
img.blur(10, 5)
img.display()
img.write('lena_blur.jpg')
