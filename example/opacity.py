from pgmagick.api import Image

img = Image('lena.jpg')
img.opacity(80)
img.display()
img.write('lena_opacity80.png')
