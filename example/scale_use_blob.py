from pgmagick import Blob, Geometry, Image

blob = Blob(open('lena_std.jpg').read())
img = Image(blob, Geometry(200, 200))
img.scale('200x200')
img.quality(95)
img.write('lena_scale.jpg')
