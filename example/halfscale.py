from pgmagick.api import Image

img = Image('gradient.png')
img.scale(0.5, 'lanczos')
img.display()
img.write('halfscale.png')
