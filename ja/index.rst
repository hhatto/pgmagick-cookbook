:tocdepth: 2

pgmagickクックブック
====================
GraphicsMagickのPythonバインディングであるpgmagickの使用例を紹介します。

.. .. contents::


背景色が赤色のJPEG画像を作成する
--------------------------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image((300, 200), 'red')
    img.write('bg-red.jpg')

.. image:: _static/bg-red.jpg


透明なPNG画像を作成する
-----------------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image((300, 200), 'transparent')
    img.write('transparent.png')

.. image:: _static/transparent.png


背景色が白から黒でグラデーションとなる画像を作成する
----------------------------------------------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image((300, 200), 'gradient:#ffffff-#000000')
    img.write('gradient.png')

.. image:: _static/gradient.png


文字を描く
----------

.. code-block:: python

    from pgmagick.api import Image

    img = Image((300, 200))
    img.annotate('Hello World')
    img.write('helloworld.png')

.. image:: _static/helloworld.png


斜め45度に傾いた文字を描く
--------------------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image((300, 200))
    img.annotate('Hello World', angle=45)
    img.write('helloworld45.png')

.. image:: _static/helloworld45.png


日本語を描く
------------
日本語フォントを設定して、日本語をannotate()に渡します。

.. code-block:: python

    # coding: utf-8
    from pgmagick.api import Image

    img = Image((300, 200))
    img.font("/usr/share/fonts/truetype/ttf-japanese-gothic.ttf")
    img.annotate('Hello World')
    img.annotate('ようこそpgmagickへ!!')
    img.write('japanese-text.png')

.. image:: _static/japanese-text.png


画像を縮小する
--------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image('gradient.png')
    img.scale(0.5)
    img.write('halfscale.png')

.. image:: _static/halfscale.png

.. code-block:: python

    from pgmagick.api import Image

    img = Image('gradient.png')
    img.scale((150, 100), 'lanczos')
    img.write('halfscale2.png')

.. image:: _static/halfscale2.png


画像を縮小する(JPEG)
-----------------------

.. code-block:: python

    from pgmagick import Image, Blob

    img = Image(Blob(open('lena_std.jpg').read()), Geometry(200, 200))
    img.scale('200x200')
    img.write('lena_scale.jpg')

.. image:: _static/lena_std.jpg
.. image:: _static/lena_scale.jpg



サイズを取得する
----------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image((300, 200))
    print img.columns(), img.rows()
    print img.width, img.height


.. code-block:: bash

    $ python size.py
    300 200
    300 200


画像を半透明にする
------------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image('lena.jpg')
    img.opacity(80)
    img.write('lena_opacity80.jpg')

.. image:: _static/lena.jpg
.. image:: _static/lena_opacity80.png


シャープマスクをかける
----------------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image('lena.jpg')
    img.sharpen(1)
    img.write('lena_sharpen1.jpg')

.. image:: _static/lena.jpg
.. image:: _static/lena_sharpen1.jpg


画像をぼかす
------------

.. code-block:: python

    from pgmagick.api import Image

    img = Image('lena.jpg')
    img.blur(10, 5)
    img.write('lena_blur.jpg')

.. image:: _static/lena.jpg
.. image:: _static/lena_blur.jpg


エッジ抽出
----------

.. code-block:: python

    from pgmagick.api import Image

    img = Image('lena.jpg')
    img.edge(2)
    img.write('lena_edge.jpg')

.. image:: _static/lena.jpg
.. image:: _static/lena_edge.jpg


EXIF情報を削除する
------------------

.. code-block:: python

    import sys
    from pgmagick import Image, Blob

    blob = Blob()
    img = Image('exif-sample.jpg')
    img.profile("*", blob)
    img.write('exif-strip.jpg')

.. image:: _static/exif-sample.jpg
    :width: 300px
.. image:: _static/exif-strip.jpg
    :width: 300px
